import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Main {
    static {
        System.setProperty(
                "webdriver.chrome.driver",
                "C:\\Users\\maxzhivyn\\IdeaProjects\\ahw\\antara11\\src\\main\\resources\\chromedriver_win32\\chromedriver.exe"
        );
    }

    static WebDriver driver = new ChromeDriver();

    public static void main(String[] args) throws InterruptedException {
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.get("https://www.avito.ru/");

        Select categorySelect = new Select(driver.findElement(By.cssSelector("#category")));
        categorySelect.selectByVisibleText("Оргтехника и расходники");

        WebElement searchWebElement = driver.findElement(By.xpath("//input[@id='search']"));
        searchWebElement.sendKeys("Принтер");

        WebElement regionWebElement = driver.findElement(By.xpath("//div[contains(@data-marker, 'search-form/region')]/div"));
        regionWebElement.click();

        WebElement regionSearchWebElement = driver.findElement(By.xpath("//div[contains(@data-marker, 'popup-location/region')]/input"));
        regionSearchWebElement.sendKeys("Владивосток");
        Thread.sleep(1000);

        WebElement buttonRegionSaveWebElement = driver.findElement(By.xpath("//button[contains(@data-marker, 'popup-location/save-button')]"));
        buttonRegionSaveWebElement.click();

        WebElement buttonSearchWebElement = driver.findElement(By.xpath("//button[contains(@data-marker, 'search-form/submit-button')]"));
        buttonSearchWebElement.click();

        Select sortSelect = new Select(driver.findElement(By.xpath("//div[@class='form-select-v2 sort-select-3QxXG']/select")));
        sortSelect.selectByVisibleText("Дороже");

        List<WebElement> offerWebElements = driver.findElements(By.xpath("//div[@class='snippet-list js-catalog_serp']/div"));

        for (WebElement webElement : offerWebElements) {
            List<WebElement> elements = webElement.findElements(By.xpath(".//span[@itemprop='name' or @itemprop='offers']"));
            elements.forEach(we -> System.out.println(we.getText()));
            System.out.println();
        }

        driver.close();
    }
}
