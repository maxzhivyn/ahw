public class Elephant extends Herbivore implements GoodBigAnimal {
    public Elephant(String name) {
        super(name);
        aviraySize = AviraySize.BIG;
    }

    @Override
    protected String makeVoice() {
        return "PTHRRRRR";
    }


    @Override
    public void doBigTopTop() {
        logger.info(getName() + ": ТОП ТОП ТОП");
    }

    @Override
    public void attack() {
        logger.info(getName() + ": в атаку " + makeVoice());
    }
}
