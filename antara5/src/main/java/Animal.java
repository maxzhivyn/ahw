import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Animal {
    private final String name;
    public AviraySize aviraySize;
    protected final Logger logger;

    public Animal(String name) {
        this.name = name;
        this.logger = LoggerFactory.getLogger(this.getClass());
    }

    public String getName() {
        return name;
    }

    public void Eat(Food food) throws Exception {
        if (!IsGoodFood(food)) {
            throw new WorngFoodException(this.toString() + "нелья накормить " + food.toString());
        }

        logger.info(this.toString() + " поел(а) и сказал(а) " + makeVoice());
    }

    protected abstract String makeVoice();
    protected abstract boolean IsGoodFood(Food food);
}
