public class Tiger extends Predator implements GoodTiger, GoodBigAnimal {

    public Tiger(String name) {
        super(name);
        aviraySize = AviraySize.SMALL;
    }

    @Override
    protected String makeVoice() {
        return "RRRRAAAAW";
    }

    @Override
    public void doBigTopTop() {
        logger.info(getName() + ": топ топ");
    }

    @Override
    public void attack() {
        logger.info(getName() + ": в атаку");
    }

    @Override
    public void play() {
        logger.info(getName() + ": играет");
    }
}
