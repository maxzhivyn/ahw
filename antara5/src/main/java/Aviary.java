import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Aviary<T extends Animal> {
    private final int size;
    private final HashMap<String, T> animals;
    protected AviraySize aviraySize;
    private Logger logger;

    public Aviary(int size, AviraySize aviraySize) {
        this.size = size;
        this.animals = new HashMap<>();
        this.aviraySize = aviraySize;
        this.logger = LoggerFactory.getLogger(this.getClass());
    }

    public void addAnimal(T animal) {
        if (this.aviraySize != animal.aviraySize) {
            logger.error("Неподходящий вальер для данного живтного " + animal.toString() +
                    " (animal: " + animal.aviraySize + ", aviary: " + this.aviraySize + ")");

            return;
        }

        if (animals.size() < size) {
            animals.put(animal.getName(), animal);
        } else {
            System.out.println("Вальер запллнен");
        }
    }

    public void removeAnimal(String name) {
        if (animals.containsKey(name)) {
            animals.remove(name);
        } else {
            System.out.println("В вальере нет такого животного");
        }
    }

    public T getAnimal(String name) {
        return animals.getOrDefault(name, null);
    }

}
