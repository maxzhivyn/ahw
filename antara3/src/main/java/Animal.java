public abstract class Animal {
    private final String name;
    public AviraySize aviraySize;

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void Eat(Food food) throws Exception {
        if (!IsGoodFood(food)) {
            throw new WorngFoodException(this.toString() + "нелья накормить " + food.toString());
        }

        System.out.println(this.toString() + " поел(а) и сказал(а) " + makeVoice());
    }

    protected abstract String makeVoice();
    protected abstract boolean IsGoodFood(Food food);
}
