public class Cow extends Herbivore implements GoodArtiodactyl {

    public Cow(String name) {
        super(name);

        aviraySize = AviraySize.BIG;
    }



    @Override
    protected String makeVoice() {
        return "Muuuuuu";
    }

    @Override
    public void getMilk() {
        System.out.println("Молоко от " + getName() + " получено");
    }

    @Override
    public void doSunChill() {
        System.out.println("Как же мне(" + getName() + ") хорошо на солнышке");
    }
}
