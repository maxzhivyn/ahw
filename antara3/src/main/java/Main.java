import com.sun.javaws.IconUtil;

import java.util.Map;

public class Main {
    public static void main(String[] args) throws Exception {
        //  Создал 4 вальера
        Aviary<Animal> animalAviary = new Aviary<>(5, AviraySize.VERYBIG);
        Aviary<Herbivore> herbivoreAviary = new Aviary<>(5, AviraySize.BIG);
        Aviary<Predator> predatorAviary = new Aviary<>(5, AviraySize.MIDDLE);
        Aviary<Leopard> leopardAviary = new Aviary<>(5, AviraySize.SMALL);

        //  Наполнение животными
        animalAviary.addAnimal(new Leopard("Жан"));
        animalAviary.addAnimal(new Deer("Степан"));

        herbivoreAviary.addAnimal(new Cow("Буренка"));
        herbivoreAviary.addAnimal(new Deer("Егор"));
        herbivoreAviary.addAnimal(new Elephant("Федор"));
        herbivoreAviary.addAnimal(new Cow("Малышка"));

        predatorAviary.addAnimal(new Tiger("Коля"));
        predatorAviary.addAnimal(new Leopard("Станислав"));

        leopardAviary.addAnimal(new Leopard("Соня"));
        leopardAviary.addAnimal(new Leopard("Саня"));

        //  корм
        herbivoreAviary.getAnimal("Егор").Eat(new HerbivoreFood());
        herbivoreAviary.getAnimal("Федор").Eat(new HerbivoreFood());

        //  удаление животных
        herbivoreAviary.removeAnimal("Егор");

    }
}
