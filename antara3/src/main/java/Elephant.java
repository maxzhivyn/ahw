public class Elephant extends Herbivore implements GoodBigAnimal {
    public Elephant(String name) {
        super(name);
        aviraySize = AviraySize.BIG;
    }

    @Override
    protected String makeVoice() {
        return "PTHRRRRR";
    }


    @Override
    public void doBigTopTop() {
        System.out.println(getName() + ": ТОП ТОП ТОП");
    }

    @Override
    public void attack() {
        System.out.println(getName() + ": в атаку " + makeVoice());
    }
}
