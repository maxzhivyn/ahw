public abstract class Predator extends Animal {

    public Predator(String name) {
        super(name);
    }

    @Override
    protected boolean IsGoodFood(Food food) {
        return food instanceof PredatorFood;
    }
}
