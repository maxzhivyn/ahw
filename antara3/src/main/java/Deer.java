public class Deer extends Herbivore implements GoodArtiodactyl {
    public Deer(String name) {
        super(name);
        aviraySize = AviraySize.BIG;
    }

    @Override
    protected String makeVoice() {
        return "UUUUUUUUUU";
    }

    @Override
    public void getMilk() {
        System.out.println("Молоко от " + getName() + " получено");
    }

    @Override
    public void doSunChill() {
        System.out.println("Как же мне(" + getName() + ") хорошо на солнышке");
    }
}
