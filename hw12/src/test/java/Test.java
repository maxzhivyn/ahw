import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/features",//путь до feature файлов
        glue = "/scripts",//название пакета с шагами
        tags = "@1", //Теги по которым будет запускаться сценарий
        dryRun = false
)
public class Test {
    @org.junit.Test
    public static void run(String[] args) {

    }
}
