package scripts;

public enum OffersSortEnum {
    ПО_УМОЛЧАНИЮ("По умолчанию"),
    ДЕШЕВЛЕ("Дешевле"),
    ДОРОЖЕ("Дороже"),
    ПО_ДАТЕ("По дате");


    private String value;

    OffersSortEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
