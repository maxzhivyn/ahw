package scripts;

import io.cucumber.java.ParameterType;
import io.cucumber.java.ru.И;
import io.cucumber.java.ru.Пусть;
import io.cucumber.java.ru.Тогда;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class AvitoFind {
    public static final WebDriver driver;

    static {
        System.setProperty(
                "webdriver.chrome.driver",
                "C:\\Users\\maxzhivyn\\IdeaProjects\\ahw\\antara11\\src\\main\\resources\\chromedriver_win32\\chromedriver.exe"
        );

        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    @ParameterType(".*")
    public CategoryEnum categoryEnum(String categoryValue) {
        return CategoryEnum.valueOf(categoryValue);
    }

    @ParameterType(".*")
    public OffersSortEnum offersSortEnum(String offersSortValue) {
        return OffersSortEnum.valueOf(offersSortValue);
    }

    @Пусть("открыт ресурс авито")
    public static void openAvito() {
        driver.manage().window().maximize();
        driver.get("https://www.avito.ru/");
    }

    @И("в выпадающем списке категорий выбрана {categoryEnum}")
    public static void selectCategory(CategoryEnum category) {
        Select categorySelect = new Select(driver.findElement(By.cssSelector("#category")));
        categorySelect.selectByVisibleText(category.getValue());
    }

    @И("в поле поиска введено значение {word}")
    public static void searchInput(String value) {
        WebElement searchWebElement = driver.findElement(By.xpath("//input[@id='search']"));
        searchWebElement.sendKeys(value);
    }

    @Тогда("кликнуть по выпадающему списку региона")
    public static void regionClick() {
        WebElement regionWebElement = driver.findElement(By.xpath("//div[contains(@data-marker, 'search-form/region')]/div"));
        regionWebElement.click();
    }

    @Тогда("в поле регион введено значение {word}")
    public static void regionSelect(String region) {
        WebElement regionSearchWebElement = driver.findElement(By.xpath("//div[contains(@data-marker, 'popup-location/region')]/input"));
        regionSearchWebElement.sendKeys(region);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @И("нажата кнопка показать объявления")
    public static void searchButtonClick() {
        WebElement buttonRegionSaveWebElement = driver.findElement(
                By.xpath("//button[contains(@data-marker, 'popup-location/save-button')]"));

        buttonRegionSaveWebElement.click();
    }

    @Тогда("открылась страница результаты по запросу принтер")
    public static void windowOpen() {
        WebElement buttonSearchWebElement = driver.findElement(By.xpath("//button[contains(@data-marker, 'search-form/submit-button')]"));
        buttonSearchWebElement.click();
    }

    @И("активирован чекбокс только с фотографией")
    public static void checkboxActivate() {
        WebElement photoOnlyCheckbox = driver.findElement(By.xpath(" //div[@class='filters-root-3Q1ZY']/label[@class='checkbox-checkbox-7igZ6 checkbox-size-s-yHrZq']/span"));
        photoOnlyCheckbox.click();

        WebElement buttonSearchWebElement = driver.findElement(By.xpath("//button[contains(@data-marker, 'search-form/submit-button')]"));
        buttonSearchWebElement.click();
    }

    @И("в выпадающем списке сортировка выбрано значение {offersSortEnum}")
    public static void resultSort(OffersSortEnum offersSort) {
        Select sortSelect = new Select(driver.findElement(By.xpath("//div[@class='form-select-v2 sort-select-3QxXG']/select")));
        sortSelect.selectByVisibleText(offersSort.getValue());
    }

    @И("в консоль выведено значение названия и цены {int} первых товаров")
    public static void resultPrint(int count) {
        List<WebElement> offerWebElements = driver.findElements(By.xpath("//div[@class='snippet-list js-catalog_serp']/div"));

        for (int i = 1; i < count + 1; ++i) {
            List<WebElement> elements = offerWebElements.get(i).findElements(By.xpath(".//span[@itemprop='name' or @itemprop='offers']"));
            elements.forEach(we -> System.out.println(we.getText()));
            System.out.println();
        }

        driver.close();
    }
}
