package scripts;

public enum CategoryEnum {
    АУДИО_И_ВИДЕО("Аудио и Видео"),
    НАСТОЛЬНЫЕ_КОМПЬЮТЕРЫ("Настольные компьютеры"),
    НОУТБУКИ("Ноутбуки"),
    ОРГТЕХНИКА_И_РАСХОДНИКИ("Оргтехника и расходники"),
    ТЕЛЕФОНЫ("Телефоны"),
    ФОТОТЕХНИКА("Фототехника");

    private String value;

    CategoryEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
