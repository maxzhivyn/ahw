public enum AviraySize {
    SMALL(100),
    MIDDLE(250),
    BIG(500),
    VERYBIG(1000);

    private final int squareMeter;
    AviraySize(int squareMeter) {
        this.squareMeter = squareMeter;
    }

    public int getSquareMeter() {
        return squareMeter;
    }
}
