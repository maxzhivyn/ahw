public class Tiger extends Predator implements GoodTiger, GoodBigAnimal {

    public Tiger(String name) {
        super(name);
        aviraySize = AviraySize.SMALL;
    }

    @Override
    protected String makeVoice() {
        return "RRRRAAAAW";
    }

    @Override
    public void doBigTopTop() {
        System.out.println(getName() + ": топ топ");
    }

    @Override
    public void attack() {
        System.out.println(getName() + ": в атаку");
    }

    @Override
    public void play() {
        System.out.println(getName() + ": играет");
    }
}
