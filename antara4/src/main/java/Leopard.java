public class Leopard extends Predator {
    public Leopard(String name) {
        super(name);
        aviraySize = AviraySize.SMALL;
    }

    @Override
    protected String makeVoice() {
        return "RRRRRRRRRR mey";
    }
}
