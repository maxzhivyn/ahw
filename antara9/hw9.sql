--Домашнее задание №9. SQL


--1. Вывести список самолетов с кодами 320, 321, 733;
SELECT * FROM  LANIT.AIRCRAFTS_DATA ad 
WHERE AIRCRAFT_CODE IN('320', '321', '733')


--2. Вывести список самолетов с кодом не на 3;
SELECT * FROM  LANIT.AIRCRAFTS_DATA ad 
WHERE AIRCRAFT_CODE NOT LIKE '3%'

--3. Найти билеты оформленные на имя «OLGA», с емайлом «OLGA» или без емайла;
SELECT * FROM  LANIT.TICKETS t 
WHERE UPPER(t.PASSENGER_NAME) LIKE 'OLGA%'
AND (UPPER(t.EMAIL) LIKE '%OLGA%' 
OR t.EMAIL = NULL)

--4. Найти самолеты с дальностью полета 5600, 5700. Отсортировать список по убыванию дальности полета;
SELECT * FROM LANIT.AIRCRAFTS_DATA ad 
WHERE ad."RANGE" IN('5600', '5700')
ORDER BY ad."RANGE" DESC 

--5. Найти аэропорты в Moscow. Вывести название аэропорта вместе с городом. Отсортировать по полученному названию;
SELECT (ad.AIRPORT_NAME || ' ' || ad.CITY) airCity FROM LANIT.AIRPORTS_DATA ad
WHERE UPPER(ad.CITY ) = 'MOSCOW'
ORDER BY airCity DESC

--6. Вывести список всех городов без повторов в зоне «Europe»;
SELECT DISTINCT ad.CITY FROM LANIT.AIRPORTS_DATA ad 
WHERE ad.TIMEZONE LIKE 'Europe%'

--7. Найти бронирование с кодом на «3A4» и вывести сумму брони со скидкой 10%
SELECT b.BOOK_REF, b.TOTAL_AMOUNT*0.9 FROM LANIT.BOOKINGS b 
WHERE UPPER(b.BOOK_REF) LIKE '3A4%'

--8. Вывести все данные по местам в самолете с кодом 320 и классом «Business»в формате «Данные по месту: номер места»
SELECT s.AIRCRAFT_CODE, s.FARE_CONDITIONS, s.SEAT_NO FROM LANIT.SEATS s 
WHERE s.AIRCRAFT_CODE = '320'
AND s.FARE_CONDITIONS = 'Business'

--9. Найти максимальную и минимальную сумму бронирования в 2017 году;
SELECT MIN(b.TOTAL_AMOUNT) min, MAX(b.TOTAL_AMOUNT) max FROM LANIT.BOOKINGS b 
WHERE TRUNC(b.BOOK_DATE, 'YYYY') = '01.01.2017'


--10. Найти количество мест во всех самолетах;
SELECT s.AIRCRAFT_CODE, COUNT(s.SEAT_NO) FROM LANIT.SEATS s 
GROUP BY s.AIRCRAFT_CODE 

--11. Найти количество мест во всех самолетах с учетом типа места;
SELECT s.AIRCRAFT_CODE, s.FARE_CONDITIONS, COUNT(s.SEAT_NO) FROM LANIT.SEATS s 
GROUP BY s.AIRCRAFT_CODE, s.FARE_CONDITIONS  

--12. Найти количество билетов пассажира ALEKSANDR STEPANOV, телефон которого заканчивается на 11;
SELECT COUNT(t.TICKET_NO ) FROM LANIT.TICKETS t 
WHERE UPPER(t.PASSENGER_NAME) = 'ALEKSANDR STEPANOV'
AND t.PHONE LIKE '%11'


--13. Вывести всех пассажиров с именем ALEKSANDR, у которых количество билетов больше 2000. Отсортировать по убыванию количества билетов;
SELECT t.PASSENGER_NAME, COUNT(*) c FROM LANIT.TICKETS t 
WHERE UPPER(t.PASSENGER_NAME) LIKE 'ALEKSANDR%'
GROUP BY t.PASSENGER_NAME
HAVING COUNT(*) > 2000
ORDER BY c DESC 

--14. Вывести дни в сентябре 2017 с количеством рейсов больше 500.
SELECT TRUNC(f.DATE_DEPARTURE) DATE_DEPARTURE, COUNT(*) countFlight 
FROM LANIT.FLIGHTS f 
WHERE TRUNC(f.DATE_DEPARTURE, 'MM') = TO_DATE('01.09.2017', 'DD.MM.YYYY')
GROUP BY TRUNC(f.DATE_DEPARTURE)
HAVING COUNT(*) > 500
ORDER BY 1

--15. Вывести список городов, в которых несколько аэропортов
SELECT ad.CITY 
FROM LANIT.AIRPORTS_DATA ad
GROUP BY ad.CITY 
HAVING COUNT(*) > 1

