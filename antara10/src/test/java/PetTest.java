import groovy.transform.ASTTest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.apache.commons.lang3.RandomStringUtils;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

import static io.restassured.module.jsv.JsonSchemaValidator.*;

public class PetTest {

    @Test
    public void createResourcePostTest() {
        String idTestValue = RandomStringUtils.randomNumeric(5);

        RestAssured.given()
                .baseUri("https://petstore.swagger.io")
                .basePath("/v2/pet")
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "  \"id\": " + idTestValue + ",\n" +
                        "  \"category\": {\n" +
                        "    \"id\": 0,\n" +
                        "    \"name\": \"string\"\n" +
                        "  },\n" +
                        "  \"name\": \"lol\",\n" +
                        "  \"photoUrls\": [\n" +
                        "    \"string\"\n" +
                        "  ],\n" +
                        "  \"tags\": [\n" +
                        "    {\n" +
                        "      \"id\": 0,\n" +
                        "      \"name\": \"string\"\n" +
                        "    }\n" +
                        "  ],\n" +
                        "  \"status\": \"available\"\n" +
                        "}")
                .when().post()
                .then()
                .statusCode(200)
                .body("id", equalTo(Integer.valueOf(idTestValue)));
    }

    @Test
    public void modificationResourcePutTest() {
        RestAssured.given()
                .baseUri("https://petstore.swagger.io")
                .basePath("/v2/pet")
                .contentType(ContentType.JSON)
                .header("api_key", "myapikeywow")
                .body("{\n" +
                        "  \"id\": 1,\n" +
                        "  \"category\": {\n" +
                        "    \"id\": 0,\n" +
                        "    \"name\": \"string\"\n" +
                        "  },\n" +
                        "  \"name\": \"leopard\",\n" +
                        "  \"photoUrls\": [\n" +
                        "    \"string\"\n" +
                        "  ],\n" +
                        "  \"tags\": [\n" +
                        "    {\n" +
                        "      \"id\": 0,\n" +
                        "      \"name\": \"string\"\n" +
                        "    }\n" +
                        "  ],\n" +
                        "  \"status\": \"available\"\n" +
                        "}")
                .when().put()
                .then()
                .statusCode(200)
                .body("name", equalTo("leopard"));
    }

    @Test
    public void modificationResourcePostTest() {
        // модификации ресурса POST
        Map<String, Object> map = new HashMap<String, Object>();
        File img = new File("src/test/resources/img.jpg");
        map.put("additionalMetadata", "information");
        map.put("file", img);

        RestAssured.given()
                .params(map)
                .when().post("https://petstore.swagger.io/v2/pet/1")
                .then()
                .statusCode(200);
    }

    @Test
    public void deleteResourceDeleteTest() throws IOException {
        RestAssured.given()
                .header("api_key", "pass")
                .when().delete("https://petstore.swagger.io/v2/pet/1")
                .then()
                .statusCode(200);
    }

    @Test
    public void deleteResourcePostTest() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("name", "RAT");
        map.put("status", "ok");

        RestAssured.given()
                .params(map)
                .when().post("https://petstore.swagger.io/v2/pet/1")
                .then()
                .statusCode(200);
    }

    @Test
    public void badCodeGetTest() {
        RestAssured.given()
                .contentType(ContentType.JSON)
                .when().get("https://petstore.swagger.io/v2/pet/1")
                .then()
                .statusCode(200)
                .body("name", equalTo("doggie"));


        RestAssured.given()
                .contentType(ContentType.JSON)
                .when().get("https://petstore.swagger.io/v2/pet/sss")
                .then()
                .statusCode(404);

        RestAssured.given()
                .contentType(ContentType.JSON)
                .when().get("https://petstore.swagger.io/v2/pet/findByStatus?status=available")
                .then()
                .statusCode(200);

        // запрос верный, но тк такого статуса нет вернул пустой json
        RestAssured.given()
                .contentType(ContentType.JSON)
                .when().get("https://petstore.swagger.io/v2/pet/findByStat?lol=1111111")
                .then()
                .statusCode(404)
                .extract().response().prettyPrint();

    }
}
