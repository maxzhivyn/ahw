import com.fasterxml.jackson.annotation.JsonProperty;

public class Tiger extends Predator implements GoodTiger, GoodBigAnimal {

    public Tiger(@JsonProperty("name") String name) {
        super(name);
        aviraySize = AviraySize.MIDDLE;
    }

    @Override
    protected String makeVoice() {
        return "RRRRAAAAW";
    }

    @Override
    public void doBigTopTop() {
        logger.info(getName() + ": топ топ");
    }

    @Override
    public void attack() {
        logger.info(getName() + ": в атаку");
    }

    @Override
    public void play() {
        logger.info(getName() + ": играет");
    }
}
