import com.fasterxml.jackson.annotation.JsonProperty;

public class Leopard extends Predator {
    public Leopard(@JsonProperty("name") String name) {
        super(name);
        aviraySize = AviraySize.MIDDLE;
    }

    @Override
    protected String makeVoice() {
        return "RRRRRRRRRR mey";
    }
}
