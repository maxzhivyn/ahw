import com.fasterxml.jackson.annotation.JsonProperty;

public class Cow extends Herbivore implements GoodArtiodactyl {

    public Cow(@JsonProperty("name") String name) {
        super(name);

        aviraySize = AviraySize.BIG;
    }

    @Override
    protected String makeVoice() {
        return "Muuuuuu";
    }

    @Override
    public void getMilk() {
        logger.info("Молоко от " + getName() + " получено");
    }

    @Override
    public void doSunChill() {
        logger.info("Как же мне(" + getName() + ") хорошо на солнышке");
    }
}
