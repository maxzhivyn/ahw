import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class Predator extends Animal {

    public Predator(@JsonProperty("name") String name) {
        super(name);
    }

    @Override
    protected boolean IsGoodFood(Food food) {
        return food.getClass().getName().equals(PredatorFood.class.getName());
    }
}
