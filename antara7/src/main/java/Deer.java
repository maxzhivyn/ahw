import com.fasterxml.jackson.annotation.JsonProperty;

public class Deer extends Herbivore implements GoodArtiodactyl {
    public Deer(@JsonProperty("name") String name) {
        super(name);
        aviraySize = AviraySize.BIG;
    }

    @Override
    protected String makeVoice() {
        return "UUUUUUUUUU";
    }

    @Override
    public void getMilk() {
        logger.info("Молоко от " + getName() + " получено");
    }

    @Override
    public void doSunChill() {
        logger.info("Как же мне(" + getName() + ") хорошо на солнышке");
    }
}
