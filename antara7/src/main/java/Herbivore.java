import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class Herbivore extends Animal {
    public Herbivore(@JsonProperty("name") String name) {

        super(name);
    }

    @Override
    protected boolean IsGoodFood(Food food) {
        return food.getClass().getName().equals(HerbivoreFood.class.getName());
    }
}
