import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
@JsonIgnoreProperties({ "logger" })
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
public class Aviary<T extends Animal> {
    private final int size;
    private final HashMap<String, T> animals;
    protected AviraySize aviraySize;

    private final Logger logger;

    public Aviary(@JsonProperty("size") int size, @JsonProperty("aviraySize") AviraySize aviraySize) {
        this.size = size;
        this.animals = new HashMap<>();
        this.aviraySize = aviraySize;
        this.logger = LoggerFactory.getLogger(this.getClass());
    }

    public void addAnimal(T animal) {
        if (this.aviraySize != animal.aviraySize) {
            logger.error("Неподходящий вальер для данного живтного " + animal.toString() +
                    " (animal: " + animal.aviraySize + ", aviary: " + this.aviraySize + ")");
            return;
        }

        if (animals.size() < size) {
            animals.put(animal.getName(), animal);
        } else {
            logger.info("Вальер запллнен");
        }
    }

    public void removeAnimal(String name) {
        if (animals.containsKey(name)) {
            animals.remove(name);
        } else {
            logger.info("В вальере нет такого животного");
        }
    }
    public T getAnimal(String name) {
        return animals.getOrDefault(name, null);
    }

    public int getSize() {
        return size;
    }

    public HashMap<String, T> getAnimals() {
        return animals;
    }

    public AviraySize getAviraySize() {
        return aviraySize;
    }

}
