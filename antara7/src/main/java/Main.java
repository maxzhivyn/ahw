import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.File;

public class Main {
    public static void main(String[] args) throws Exception {
        //  Создал 4 вальера
        XmlMapper xmlMapper = new XmlMapper();
        File file = new File("dump.xml");

        Zoo zoo = (file.length() == 0)
                ? new Zoo()
                : xmlMapper.readValue(file, Zoo.class);

        ((Aviary<Animal>) zoo.getAviary(Predator.class.getName())).addAnimal(new Tiger("Пауль"));
        ((Aviary<Animal>) zoo.getAviary(Predator.class.getName())).addAnimal(new Tiger("Пуль"));


//        //  Создание вальеров
//        zoo.addAviary(Animal.class.getName(), new Aviary<Animal>(5, AviraySize.BIG));
//        zoo.addAviary(Herbivore.class.getName(), new Aviary<Herbivore>(5, AviraySize.BIG));
//        zoo.addAviary(Predator.class.getName(), new Aviary<Predator>(5, AviraySize.MIDDLE));
//        zoo.addAviary(Elephant.class.getName(), new Aviary<Elephant>(5, AviraySize.VERYBIG));
//
//
//        //  Наполнение животными
//        ((Aviary<Animal>) zoo.getAviary(Animal.class.getName())).addAnimal(new Cow("Буранка"));
//        ((Aviary<Animal>) zoo.getAviary(Animal.class.getName())).addAnimal(new Deer("Олег"));
//
//        ((Aviary<Animal>) zoo.getAviary(Herbivore.class.getName())).addAnimal(new Cow("Буренка"));
//        ((Aviary<Animal>) zoo.getAviary(Herbivore.class.getName())).addAnimal(new Deer("Егор"));
//        ((Aviary<Animal>) zoo.getAviary(Herbivore.class.getName())).addAnimal(new Cow("Маха"));
//        ((Aviary<Animal>) zoo.getAviary(Herbivore.class.getName())).addAnimal(new Elephant("Федор")); // не вствится по размеру
//
//        ((Aviary<Animal>) zoo.getAviary(Predator.class.getName())).addAnimal(new Tiger("Коля"));
//        ((Aviary<Animal>) zoo.getAviary(Predator.class.getName())).addAnimal(new Leopard("Стан"));
//
//        ((Aviary<Animal>) zoo.getAviary(Elephant.class.getName())).addAnimal(new Elephant("Соня"));
//        ((Aviary<Animal>) zoo.getAviary(Elephant.class.getName())).addAnimal(new Elephant("Саня")); // не вствится по размеру
//
//        // корм
//        zoo.getAviary(Herbivore.class.getName()).getAnimal("Маха").eat(new HerbivoreFood());
//        zoo.getAviary(Herbivore.class.getName()).getAnimal("Егор").eat(new HerbivoreFood());
//
//        // удаление животных
//        zoo.getAviary(Herbivore.class.getName()).removeAnimal("Егор");

        xmlMapper.writeValue(file, zoo);

    }

}
