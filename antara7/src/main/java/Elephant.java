import com.fasterxml.jackson.annotation.JsonProperty;

public class Elephant extends Herbivore implements GoodBigAnimal {
    public Elephant(@JsonProperty("name") String name) {
        super(name);
        aviraySize = AviraySize.VERYBIG;
    }

    @Override
    protected String makeVoice() {
        return "PTHRRRRR";
    }


    @Override
    public void doBigTopTop() {
        logger.info(getName() + ": ТОП ТОП ТОП");
    }

    @Override
    public void attack() {
        logger.info(getName() + ": в атаку " + makeVoice());
    }
}
