import com.fasterxml.jackson.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
@JsonIgnoreProperties({ "logger" })
public abstract class Animal {
    public AviraySize aviraySize;
    private final String name;

    protected final Logger logger;

    public Animal(@JsonProperty("name") String name) {
        this.name = name;
        this.logger = LoggerFactory.getLogger(this.getClass());
    }

    public void eat(Food food) throws Exception {
        if (!IsGoodFood(food)) {
            throw new WorngFoodException(this.toString() + "нелья накормить " + food.toString());
        }

        logger.info(this.toString() + " поел(а) и сказал(а) " + makeVoice());
    }

    public String getName() {
        return name;
    }

    protected abstract String makeVoice();
    protected abstract boolean IsGoodFood(Food food);
}
