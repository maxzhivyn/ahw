import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.HashMap;
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
public class Zoo {
    private final HashMap<String, Aviary<?  extends Animal>> aviaries;

    public Zoo() {
        this.aviaries = new HashMap<>();
    }

    public HashMap<String, Aviary<? extends Animal>> getAviaries() {
        return aviaries;
    }

    public void addAviary(String name, Aviary<? extends Animal> aviary) {
        aviaries.put(name,  aviary);
    }

    public Aviary<? extends Animal> getAviary(String name) {
        return aviaries.get(name);
    }
}
