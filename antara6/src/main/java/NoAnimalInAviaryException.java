public class NoAnimalInAviaryException extends Exception {
    NoAnimalInAviaryException(String str) {
        super(str);
    }
}
