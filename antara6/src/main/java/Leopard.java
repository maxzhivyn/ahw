public class Leopard extends Predator {
    public Leopard(String name) {
        super(name);
        aviraySize = AviraySize.SMALL;
    }

    @Override
    protected String doRoar() {
        return "RRRRRRRRRR mey";
    }
}
