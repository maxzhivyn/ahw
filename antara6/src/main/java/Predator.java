public abstract class Predator extends Animal {

    public Predator(String name) {
        super(name);
    }

    @Override
    protected boolean IsGoodFood(Food food) {
        return food.getClass().getName().equals(PredatorFood.class.getName());
    }
}
