import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

public class Aviary<T extends Animal> {
    private final int size;
    private final HashMap<String, T> animals;
    protected AviraySize aviraySize;
    private Logger logger;

    public Aviary(int size, AviraySize aviraySize) {
        this.size = size;
        this.animals = new HashMap<>();
        this.aviraySize = aviraySize;
        this.logger = LoggerFactory.getLogger(this.getClass());
    }

    public void addAnimal(T animal) throws FullAviaryException, SizeAnimalTypeException {
        if (this.aviraySize != animal.aviraySize) {
            throw new SizeAnimalTypeException("Неподходящий вальер для данного живтного " + animal.toString() +
                    " (animal: " + animal.aviraySize + ", aviary: " + this.aviraySize + ")");
        }

        if (animals.size() < size) {
            animals.put(animal.getName(), animal);
        } else {
            throw new FullAviaryException("Вальер запллнен");
        }
    }

    public void removeAnimal(String name) throws NoAnimalInAviaryException {
        if (animals.containsKey(name)) {
            animals.remove(name);
        } else {
            throw new NoAnimalInAviaryException("Нет такого животног в вальере");
        }
    }

    public void doRoarAll() {
        animals.forEach((name, animal) -> {
            logger.info(name + "(" + animal.getClass() + "): " + animal.doRoar());
        });
    }

    public T getAnimal(String name) {
        return animals.getOrDefault(name, null);
    }

}
