import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    public static void main(String[] args) throws Exception {
        Logger logger = LoggerFactory.getLogger(Main.class);

        //  Создал 4 вальера
        Aviary<Animal> animalAviary = new Aviary<>(5, AviraySize.VERYBIG);
        Aviary<Herbivore> herbivoreAviary = new Aviary<>(5, AviraySize.BIG);
        Aviary<Predator> predatorAviary = new Aviary<>(5, AviraySize.MIDDLE);
        Aviary<Leopard> leopardAviary = new Aviary<>(5, AviraySize.SMALL);

        //  Наполнение животными
        animalAviary.addAnimal(new Leopard("Жан"));
        animalAviary.addAnimal(new Deer("Степан"));

        herbivoreAviary.addAnimal(new Cow("Буренка"));
        herbivoreAviary.addAnimal(new Deer("Егор"));
        herbivoreAviary.addAnimal(new Elephant("Федор"));
        herbivoreAviary.addAnimal(new Cow("Малышка"));

        predatorAviary.addAnimal(new Tiger("Коля"));
        predatorAviary.addAnimal(new Leopard("Станислав"));

        leopardAviary.addAnimal(new Leopard("Соня"));
        leopardAviary.addAnimal(new Leopard("Саня"));

        //  корм
        logger.info(herbivoreAviary.getAnimal("Егор").eat(new HerbivoreFood()));
        logger.info(herbivoreAviary.getAnimal("Федор").eat(new HerbivoreFood()));

        // Крик со всего вальера
        herbivoreAviary.doRoarAll();

        //  удаление животных
        herbivoreAviary.removeAnimal("Егор");


    }
}
