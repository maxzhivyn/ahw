public abstract class Herbivore extends Animal {
    public Herbivore(String name) {
        super(name);
    }

    @Override
    protected boolean IsGoodFood(Food food) {
        return food.getClass().getName().equals(HerbivoreFood.class.getName());
    }

    public int danger;
}
