public class Elephant extends Herbivore implements GoodBigAnimal {
    public Elephant(String name) {
        super(name);
        aviraySize = AviraySize.VERYBIG;
    }

    @Override
    protected String doRoar() {
        return "PTHRRRRR";
    }


    @Override
    public void doBigTopTop() {
        logger.info(getName() + ": ТОП ТОП ТОП");
    }

    @Override
    public void attack() {
        logger.info(getName() + ": в атаку " + doRoar());
    }
}
