import org.junit.Assert;
import org.junit.jupiter.api.Test;

class AnimalTest {
    @Test
    void eatGood() throws Exception {
        Tiger tiger = new Tiger("Вован");
        String result = tiger.eat(new PredatorFood());
        System.out.println(result);
        Assert.assertEquals(tiger.toString() + " поел(а)", result);
    }

    @Test
    void eatException() throws Exception {
        Tiger tiger = new Tiger("Вован");

        try {
            String result = tiger.eat(new HerbivoreFood());
            Assert.fail("No WorngFoodException");
        } catch (WorngFoodException e) {
            Assert.assertNotEquals("", e.getMessage());
        }
    }

    @Test
    void fullAviaryException() throws SizeAnimalTypeException {
        Aviary<Elephant> elephantAviary = new Aviary<>(3, AviraySize.VERYBIG);

        try {
            elephantAviary.addAnimal(new Elephant("el1"));
            elephantAviary.addAnimal(new Elephant("el2"));
            elephantAviary.addAnimal(new Elephant("el3"));
            elephantAviary.addAnimal(new Elephant("el4"));
            Assert.fail("No FullAviaryException");
        } catch (FullAviaryException e) {
            Assert.assertNotEquals("", e.getMessage());
        }
    }

    @Test
    void sizeAnimalTypeException() throws FullAviaryException {
        Aviary<Elephant> elephantAviary = new Aviary<>(3, AviraySize.BIG);

        try {
            elephantAviary.addAnimal(new Elephant("el1"));
            Assert.fail("No FullAviaryException");
        } catch (SizeAnimalTypeException e) {
            Assert.assertNotEquals("", e.getMessage());
        }
    }

    @Test
    void removeAnimalTest() throws FullAviaryException, SizeAnimalTypeException {
        Aviary<Herbivore> elephantAviary = new Aviary<>(3, AviraySize.BIG);
        elephantAviary.addAnimal(new Cow("CO"));
        elephantAviary.addAnimal(new Deer("DE"));

        try {
            elephantAviary.removeAnimal("BE");
            Assert.fail("No NoAnimalInAviaryException");
        } catch (NoAnimalInAviaryException e) {
            Assert.assertNotEquals("", e.getMessage());
        }
    }

}